<?php

namespace Drupal\entity_alert\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class MailConfigForm extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MailConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_alert';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_alert.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Fetch all roles.
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    // Create an array of role names for the dropdown options.
    $role_options = [];
    foreach ($roles as $role) {
      $role_options[$role->id()] = $role->label();
    }

    // Role selection dropdown with multiple selection.
    $form['selected_roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Roles'),
      '#options' => $role_options,
      '#multiple' => TRUE,
      '#default_value' => $this->config('entity_alert.settings')->get('selected_roles'),
    ];

    // Fetch all content types.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Create an array of content type names for the dropdown options.
    $content_type_options = [];
    foreach ($content_types as $content_type) {
      $content_type_options[$content_type->id()] = $content_type->label();
    }

    // Content type selection dropdown with multiple selection.
    $form['selected_content_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Types'),
      '#options' => $content_type_options,
      '#multiple' => TRUE,
      '#default_value' => $this->config('entity_alert.settings')->get('selected_content_types'),
    ];

    // Email field.
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#default_value' => $this->config('entity_alert.settings')->get('email'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration settings.
    $config = $this->config('entity_alert.settings');
    $config->set('selected_roles', $form_state->getValue('selected_roles'))
      ->set('selected_content_types', $form_state->getValue('selected_content_types'))
      ->set('email', $form_state->getValue('email'))
      ->save();

    parent::submitForm($form, $form_state);

  }

}
