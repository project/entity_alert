<?php

namespace Drupal\entity_alert\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_alert\Event\NodeUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Logs the creation of a new node.
 */
class NodeUpdateSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $configFactory;
  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;
  /**
   * {@inheritdoc}
   */
  protected $mailManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, MailManagerInterface $mailManager) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->mailManager = $mailManager;

  }

  /**
   * {@inheritdoc}
   */
  public function onDemoNodeUpdate(NodeUpdateEvent $event) {
    // Get the entity from the event.
    $entity = $event->getEntity();

    $config = $this->configFactory->get('entity_alert.settings');
    // Get specific configuration values.
    $selectedRoles = $config->get('selected_roles') ?: [];
    $selectedContentTypes = $config->get('selected_content_types') ?: [];

    // Load all users with the specified role.
    foreach ($selectedRoles as $selectedRole) {
      $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['roles' => $selectedRole]);

    }

    if ($entity->getEntityTypeId() == 'node' && in_array($entity->bundle(), $selectedContentTypes)) {

      // If ($entity->getEntityTypeId() == 'node') {
      // Prepare and send the email.
      foreach ($users as $user) {
        // You can customize the email subject and message.
        $subject = $this->t('Content updated/added: @title', ['@title' => $entity->label()]);
        $body = $this->t('Content with title "@title" has been updated/added on the site.', ['@title' => $entity->label()]);
        $params = [
          'subject' => $subject,
          'body' => $body,
        ];

        // Send the email to each user.
        $this->mailManager->mail(
          'entity_alert',
          'notification',
          $user->getEmail(),
          $user->getPreferredLangcode(),
          $params
        );

      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[NodeUpdateEvent::DEMO_NODE_UPDATE][] = ['onDemoNodeUpdate'];
    return $events;
  }

}
